"""

"""

import json
import logging
from random import choice

import client

# Text
I_WILL_FIND_TEXT = 'Скажите название песни и я попробую найти аккорды и текст песни.'\
                    'А ещё можно попробовать искать по словам. '
INTRO_TEXT = 'Привет! ' + I_WILL_FIND_TEXT + '\nПесни могут повторяться, значит есть несколько версий.'
OTHER_SONG_TEXT = 'Хорошо. ' + I_WILL_FIND_TEXT
CHORD_BLOCK_TITLE_TEXT = "Вот аккорды для '{song}'\n{text}"
SONGS_TEXT = 'Популярные песни по этому запросу:\n{songs}\n\nВыберите номер песни.'
NO_SONGS_TEXT = 'К сожалению, ничего не нашлось, попробуйте другой запрос.'
MOVE_TO_NEXT_SONG_TEXT = "Хотите чтобы я нашла слова для '{song}'?"
NO_CHORDS_PHRASES = (
    'Ой, не получилось найти. Попробуйте ещё раз.',
    'Ошибочка, давайте лучше другую песню.',
    'Что-то не получилось. Может следующую песню?',
)
GOOD_WORDS = (
    'Хорошо',
    'Так держать!',
    'Обращайтесь'
)
RETRY_WORDS = (
    'Попробуйте ещё раз',
    'Немного не понятно',
    'Повторите'
)
CHORDS_END = (
    'Конец.',
    'Ну вот и всё.',
    'Повторим?',
    'Песня кончилась.',
    'Изгиб гитары желтой... Ой, а уже всё.'
)

# Buttons titles
OTHER_SONG_BUTTON_TEXT = 'Найти другую песню'
NEXT_CHORD_BUTTON_TEXT = 'Следующий куплет'
OPEN_YA_MUSIC_BUTTON_TEXT = 'Искать песню на Яндекс.Музыке'
FIND_SONG_BUTTON_TEXT = 'Найти'

# Session keys
K_SONGS = 'songs'
K_REQUEST = 'request'
K_WAIT_CHOICE = 'wait_choice'
K_CHORDS_OFFER = 'chord_offset'
K_CHORDS = 'chords'
K_BUTTONS = 'last_buttons'

# System keys
SK_RESPONSE = 'response'
SK_TEXT = 'text'
SK_BUTTONS = 'buttons'


def button(title):
    """ Create Dialogs compatible button """
    return {
        'title': title,
        'hide': True
    }


# Buttons
OTHER_SONG_BUTTON = button(OTHER_SONG_BUTTON_TEXT)
NEXT_CHORD_BUTTON = button(NEXT_CHORD_BUTTON_TEXT)
OPEN_YA_MUSIC_BUTTON = button(OPEN_YA_MUSIC_BUTTON_TEXT)
FIND_SONG_BUTTON = button(FIND_SONG_BUTTON_TEXT)

# Логирование
logging.basicConfig(level=logging.DEBUG)
# Хранилище данных о сессиях.


class RequestHandler:
    def __init__(self):
        """ Init sessions """
        self._sessions = {}

    def alice(self, req):
        """ General handler, init response """
        logging.info('Request: %r', req.json)

        response = {
            "version": req.json['version'],
            "session": req.json['session'],
            "response": {
                "end_session": False
            }
        }

        self.handle_dialog(req.json, response)
        logging.info('Response: %r', response)

        return json.dumps(
            response,
            ensure_ascii=False,
        )

    def handle_dialog(self, req, res):
        """ Process request and fill response """
        user_id = req['session']['user_id']
        user_session = req['session']

        if user_session['new']:
            # Это новый пользователь.
            # Инициализируем сессию и поприветствуем его.
            self.initialize_user(res, user_id)
            return

        orig_utter = req[K_REQUEST]['original_utterance']

        # Пользователь хочет другую песню
        if orig_utter.lower() == OTHER_SONG_BUTTON_TEXT.lower():
            self.wait_for_other_song(res, user_id)
            return

        elif orig_utter.lower() == FIND_SONG_BUTTON_TEXT.lower():
            self.refresh(user_id)
            self.find_songs_by_text(res, user_id, orig_utter)

        elif orig_utter.lower() == OPEN_YA_MUSIC_BUTTON_TEXT.lower():
            self.open_yandex_music(res, user_id)
            return

        # Пользователь должен выбрать песню
        if self._sessions[user_id].get(K_WAIT_CHOICE, False):
            # Проверяем, если пользователь выбрал песню
            if orig_utter.lower() == NEXT_CHORD_BUTTON_TEXT.lower():
                self.get_song_chords(res, user_id)
                return

            try:
                idx = int(orig_utter)
                if idx in self._sessions[user_id][K_SONGS]:
                    self.get_song_chords(res, user_id, idx)
                    return
            except ValueError:
                pass

            if hasattr(self._sessions[user_id], K_CHORDS_OFFER):
                self.move_to_next_song(res, orig_utter)
            else:
                self.retry_song_choice(res, user_id)
            return

        # Пользователь ищет песню
        if K_SONGS not in self._sessions[user_id]:
            self.find_songs_by_text(res, user_id, orig_utter)
            return

        self.default(res, user_id)

    def initialize_user(self, res, user_id):
        """ Create session and intro text """
        self._sessions[user_id] = {}
        res[SK_RESPONSE][SK_TEXT] = INTRO_TEXT

    def refresh(self, user_id):
        """  """
        self._sessions[user_id].pop(K_SONGS, None)
        self._sessions[user_id].pop(K_CHORDS, None)
        self._sessions[user_id].pop(K_WAIT_CHOICE, None)
        self._sessions[user_id].pop(K_REQUEST, None)
        self._sessions[user_id].pop(K_BUTTONS, None)
        self._sessions[user_id].pop(K_CHORDS_OFFER, None)

    def open_yandex_music(self, res, user_id):
        """  """
        self.refresh(user_id)
        res[SK_RESPONSE][SK_BUTTONS] = [OTHER_SONG_BUTTON]

    def retry_song_choice(self, res, user_id):
        """  """
        res[SK_RESPONSE][SK_TEXT] = choice(RETRY_WORDS)
        res[SK_RESPONSE][SK_BUTTONS] = self._sessions[user_id][K_BUTTONS]

    def move_to_next_song(self, res, text):
        """  """
        res[SK_RESPONSE][SK_TEXT] = MOVE_TO_NEXT_SONG_TEXT.format(song=text)
        res[SK_RESPONSE][SK_BUTTONS] = [
            FIND_SONG_BUTTON,
            NEXT_CHORD_BUTTON,
            OTHER_SONG_BUTTON,
        ]

    def find_songs_by_text(self, res, user_id, text):
        """ Find song by name ot text """
        songs = client.get_songs(text)
        if not songs:
            res[SK_RESPONSE][SK_TEXT] = NO_SONGS_TEXT
            return

        _buttons = [
            button(str(idx)) for idx in range(1, len(songs) + 1)
        ] + [
            OTHER_SONG_BUTTON,
            self.get_yandex_music_button(text),
        ]

        self._sessions[user_id][K_REQUEST] = text
        self._sessions[user_id][K_SONGS] = songs
        self._sessions[user_id][K_WAIT_CHOICE] = True
        self._sessions[user_id][K_BUTTONS] = _buttons
        res[SK_RESPONSE][SK_BUTTONS] = _buttons
        res[SK_RESPONSE][SK_TEXT] = SONGS_TEXT.format(
            songs='\n'.join([s['name'] for s in songs.values()])
        )

    def wait_for_other_song(self, res, user_id):
        """  """
        self.refresh(user_id)
        res[SK_RESPONSE][SK_TEXT] = OTHER_SONG_TEXT

    def get_song_chords(self, res, user_id, number=None):
        """  """
        # Возвращаем первый куплет
        if number:
            self._sessions[user_id][K_CHORDS_OFFER] = 1
            song_info = self._sessions[user_id][K_SONGS][number]
            del self._sessions[user_id][K_SONGS]

            song_chords = client.get_chords(song_info)
            if not song_chords:
                res[SK_RESPONSE][SK_TEXT] = choice(NO_CHORDS_PHRASES)
                res[SK_RESPONSE][SK_BUTTONS] = [OTHER_SONG_BUTTON]
                return
            self._sessions[user_id][K_CHORDS] = song_chords
            res[SK_RESPONSE][SK_TEXT] = CHORD_BLOCK_TITLE_TEXT.format(
                song=song_info['name'],
                text=song_chords[0],
            )
        else:
            offset = self._sessions[user_id][K_CHORDS_OFFER]
            if offset < len(self._sessions[user_id][K_CHORDS]):
                res[SK_RESPONSE][SK_TEXT] = self._sessions[user_id][K_CHORDS][offset]
                self._sessions[user_id][K_CHORDS_OFFER] += 1
            else:
                res[SK_RESPONSE][SK_TEXT] = choice(CHORDS_END)
                res[SK_RESPONSE][SK_BUTTONS] = [OTHER_SONG_BUTTON]
                self.refresh(user_id)
                return

        res[SK_RESPONSE][SK_BUTTONS] = [
            NEXT_CHORD_BUTTON,
            OTHER_SONG_BUTTON,
        ]

    def default(self, res, user_id):
        res[SK_RESPONSE][SK_TEXT] = choice(RETRY_WORDS)
        res[SK_RESPONSE][SK_BUTTONS] = [OTHER_SONG_BUTTON]

    @staticmethod
    def get_yandex_music_button(text):
        btn = OPEN_YA_MUSIC_BUTTON
        btn['url'] = "https://music.yandex.ru/search?text={}".format(
            client.quote_plus(text)
        )
        return btn
