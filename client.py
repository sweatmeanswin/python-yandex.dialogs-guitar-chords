"""

"""

from urllib.parse import quote_plus

import requests
from bs4 import BeautifulSoup

SONGS_COUNT = 5
URL = 'https://amdm.ru/search/?q={query}'

session = requests.session()


def get_songs(song_name):
    response = session.get(URL.format(query=quote_plus(song_name)))
    if not response.ok:
        return 'Empty'
    response.encoding = 'utf-8'
    soup = BeautifulSoup(response.text, 'html.parser')
    if soup:
        _song_block = soup.find(
            'table', {'class': {'items'}}
        )
        if not _song_block or 'debug2' in _song_block.get('class'):
            return []
        _song_list = _song_block.find_all(
            'td', {'class': {'artist_name'}}
        )
        if _song_list:
            _song_info_data = {}
            for _idx, _song_info in enumerate(_song_list[:SONGS_COUNT]):
                _url = _song_info.find_all('a')[1].get('href')
                if 'http' not in _url:
                    _url = 'https:' + _url
                _song_info_data[_idx + 1] = {
                    'name': _song_info.text,
                    'url': _url
                }
            return _song_info_data
    return response.text


def get_chords(song_info):
    response = session.get(song_info['url'])
    if not response.ok:
        return []
    response.encoding = 'utf-8'
    soup = BeautifulSoup(response.text, 'html.parser')
    if soup:
        chord_block = soup.find('pre', {'itemprop': {'chordsBlock'}})
        if chord_block:
            text_blocks = []
            pos = 0
            while True:
                for _delim, _step in (
                    ('\n\n', 2),
                    ('\r\n\r\n', 4),
                    ('\r\n \r\n', 5),
                ):
                    block_end = chord_block.text.find(_delim, pos)
                    if -1 < block_end < 700:
                        step = _step
                        break
                else:
                    return text_blocks

                text_blocks.append(chord_block.text[pos:block_end].strip())
                pos = block_end + step

    return []
