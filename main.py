"""

"""

from flask import Flask, request

from handler import RequestHandler


# Flask приложение
app = Flask(__name__)
app_handler = RequestHandler()


@app.route("/alice-webhook/", methods=['POST'])
def handle_alice():
    return app_handler.alice(request)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8888)
